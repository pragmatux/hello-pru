include /usr/share/sdk-pru/Makefile.mk

OUT=hello-pru.out
prefix=/lib/firmware/
DESTDIR?=$(PWD)

all: $(OUT)

clean:
	rm -f $(foreach ext,asm lst map out pp,$(basename $(OUT)).$(ext))

distclean: clean
	rm -f *~

install: $(OUT)
	install -D $(OUT) \
                $(DESTDIR)$(prefix)$(OUT)


.PHONY: distclean clean all install

