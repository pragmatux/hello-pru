#include <stdint.h>
#include <pru_cfg.h>
#include "resource_table_empty.h"
#include "prugpio.h"

void main(void) {
	const uint32_t LED = USR3;
       volatile uint32_t *gpio1 = (uint32_t *)GPIO1;
       uint32_t n = 0;

	/* Enable OCP master port. */
	CT_CFG.SYSCFG_bit.STANDBY_INIT = 0;

       /* Blink the LED. This loop never exits. */
	for (n = 0; ; n++)
       {
              if (n % 2)
                     gpio1[GPIO_CLEARDATAOUT] = LED; /* off */
              else
                     gpio1[GPIO_SETDATAOUT] = LED; /* on */

		__delay_cycles(100000000);
	}

#pragma diag_push
#pragma diag_suppress 112
	__halt();
#pragma diag_pop
}

/* For the write_init_pins helper. */
#pragma DATA_SECTION(init_pins, ".init_pins")
#pragma RETAIN(init_pins)
const char init_pins[] =  
	"/sys/class/leds/beaglebone:green:usr3/trigger\0none\0" \
	"\0\0";
