hello-pru
=========

This repository contains an example PRU firmware project, showing how to use
`sdk-pru` and dpkg to manage PRU firmware images. The application itself simply
blinks LED3 at 1Hz.

Tested on `device-akna`, i.e. Beaglebone Black.

Building
--------

Build using **-aarmhf**, even though PRU is technically a foreign architecture
to Debian. Building as a binary package isn't strictly necessary, but we chose
to do so in this case in a modest attempt to keep the binary package from being
installed on architectures that are definitely, most-assuredly incompatible.


Installation/Use
----------------

Note: You must load `AM335X-PRU-RPROC-4-19-TI-00A0.dtbo` (or equivalent) at boot.

The PRU firmware is installed as `/lib/firmware/hello-pru.out`. The following
commands should get you going:

```
# echo "none" > /sys/class/leds/beaglebone\:green\:usr3/trigger
# echo "stop" > /sys/class/remoteproc/remoteproc1/state
# echo "hello-pru.out" > /sys/class/remoteproc/remoteproc1/firmware
# echo "start" > /sys/class/remoteproc/remoteproc1/state
```

LED3 should now be blinking at 1Hz/50%.
